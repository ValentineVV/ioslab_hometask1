//
//  FurnitureSet.m
//  Task2
//
//  Created by Valiantsin Vasiliavitski on 3/27/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import "FurnitureSet.h"

@implementation FurnitureSet

@synthesize price = _price;
@synthesize weight = _weight;
@synthesize materials = _materials;

-(instancetype) initWithItem:(id<Characteristics>) item {
    
    if (self = [super init]) {
        [self addItem:item];
        
    }
    
    return self;
}

+(instancetype) FurnitureSetWithItem:(id<Characteristics>) item {
    
    return [[self alloc] initWithItem:item];
}

-(void) addItem: (id) item {
    
    if (_items == nil) {
        _items = [[NSMutableArray alloc] init];
    }
    [_items addObject:item];
    _price = [self totalPrice];
    _weight = [self totalWeight];
}

-(NSSet*) includedMaterials {
    NSMutableSet* materialSet = [[NSMutableSet alloc] init];
    if (_items != nil) {
        
        for (FurnitureSet* furniture in _items) {
            
            [materialSet addObject:furniture.materials];
        }
    }
    
    return materialSet;
}

-(NSInteger)totalWeight {
    NSInteger sum = 0;
    for (FurnitureSet* furniture in _items) {
        
        sum += furniture.weight;
    }
    
    return sum;
}

-(NSInteger) totalPrice {
    
    NSInteger sum = 0;
    for (FurnitureSet* furniture in _items) {
        
        sum += furniture.price;
    }
    
    return sum;
}

-(NSArray*) itemsWithMaterial:(Material*) material {
    NSMutableArray* itemsWithMaterial = [[NSMutableArray alloc] init];
    for (FurnitureSet* furniture in _items) {
        
        if ([furniture.materials isEqualTo:material]) {
            
            [itemsWithMaterial addObject:furniture.materials];
        }
    }
    
    return itemsWithMaterial;
}

-(NSString *)description {
    NSString* desc = @"";
    for(FurnitureSet* fur in _items) {
        
        desc = [[desc stringByAppendingString:[fur description]] stringByAppendingString:@"\n"];
    }
    
    return desc;
}


@end
