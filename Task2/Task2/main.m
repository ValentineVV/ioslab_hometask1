//
//  main.m
//  Task2
//
//  Created by Valiantsin Vasiliavitski on 3/27/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FurnitureSet.h"
#import "Stool.h"
#import "Chair.h"
#import "Bed.h"
#import "Cupboard.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
    
        Material* stoolMaterial = [Material materialWithName:@"wood" color:@"brown"];
        Material* chairMaterial = [Material materialWithName:@"wood" color:@"red"];
        Material* bedMaterial = [Material materialWithName:@"metal" color:@"gray"];
        Material* cupboardMaterial = [Material materialWithName:@"wood" color:@"red"];
        
        
        
        Stool* stool1 = [Stool stoolWithLegsAmmount:4 back:YES price:100 weight:4 materials:stoolMaterial];
        Chair* chair1 = [Chair chairWithCasing:YES filler:@"cotton" price:200 weight:10 materials:chairMaterial];
        Bed* bed1 = [Bed bedWithSpringsAmmount:1000 capacity:2 price:1000 weight:20 materials:bedMaterial];
        Cupboard* cupboard1 = [Cupboard cupboardWithDoorsAmmount:3 shelvesAmmount:5 price:500 weight:40 materials:cupboardMaterial];
        
        FurnitureSet* furniture = [FurnitureSet alloc];
        
        [furniture addItem:stool1];
        [furniture addItem:chair1];
        
//        NSArray* materials = [furniture includedMaterials];
//        NSLog(@"Список материалов:");
//        NSString* mater = @"";
//        for (Material* mat in materials) {
//            mater = [[mater stringByAppendingString:[mat description]] stringByAppendingString:@"\n"];
//        }
//        NSLog(@"%@", mater);
//
//        NSLog(@"Общий вес: %ld", (long)[furniture totalWeight]);
//
//        NSLog(@"Общая цена: %ld", (long)[furniture totalPrice]);
        
        NSArray <id<Characteristics>> *items = @[furniture, bed1, cupboard1];
        
        for (id<Characteristics> item in items) {
            NSLog(@"Цена: %ld, вес: %ld", item.price, item.weight);
        }
        
    }
    return 0;
}
