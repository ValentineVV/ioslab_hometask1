//
//  Material.m
//  Task2
//
//  Created by Valiantsin Vasiliavitski on 3/27/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import "Material.h"

@implementation Material

-(instancetype) initWithName:(NSString *)name color:(NSString *)color {
    
    if(self = [super init]) {
        _name = name;
        _color = color;
    }
    
    return self;
}

+(instancetype) materialWithName:(NSString *)name color:(NSString *)color {
    
    return [[self alloc ] initWithName:name color:color];
}

-(NSString *)description {
    
    return [NSString stringWithFormat:@"Материал: %@, цвет: %@", _name, _color];
}

-(BOOL)isEqualToMaterial:(Material*)object {
    if (!object) {
        return NO;
    }
    
    BOOL haveEqualName = (!_name && !object.name) || [_name isEqualToString:object.name];
    BOOL haveEqualColor = (!_color && !object.color) || [_color isEqualToString:object.color];
    
    return haveEqualName && haveEqualColor;
}

-(BOOL)isEqual:(id)object  {
    
    if (self == object) {
        return YES;
    }
    
    if (![object isKindOfClass:[Material class]]) {
        return NO;
    }
    
    return [self isEqualToMaterial:(Material *)object];
}
-(NSUInteger)hash {
    return [self.name hash] ^ [self.color hash];
}

@end
