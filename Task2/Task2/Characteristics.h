//
//  Characteristics.h
//  Task2
//
//  Created by Valiantsin Vasiliavitski on 3/27/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Material.h"
@protocol Characteristics <NSObject>

@property (nonatomic, readonly, copy) Material *materials;
@property (nonatomic, readonly) NSInteger weight;
@property (nonatomic, readonly) NSInteger price;

@end
