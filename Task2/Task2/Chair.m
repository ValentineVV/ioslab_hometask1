//
//  Chair.m
//  Task2
//
//  Created by Valiantsin Vasiliavitski on 3/27/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import "Chair.h"

@implementation Chair

-(instancetype) initWithCasing:(BOOL)casing filler:(NSString *)filler price:(NSInteger)price weight:(NSInteger)weight materials:(Material*)materials {
    
    if (self = [super init]) {
        
        _price = price;
        _materials = materials;
        _weight = weight;
        _casing = casing;
        _filler = filler;
    }
    
    return self;
}

+(instancetype) chairWithCasing:(BOOL)casing filler:(NSString *)filler price:(NSInteger)price weight:(NSInteger)weight materials:(Material*)materials {
    
    return [[self alloc] initWithCasing:casing filler:filler price:price weight:weight materials:materials];
}

-(NSString *)description {
    
    return [NSString stringWithFormat:@"Кресло: Наличие обивки: %d, наполнитель: %@, цена: %ld, вес: %ld, %@", _casing, _filler, _price, _weight,  _materials];
}

@synthesize weight = _weight;

@synthesize materials = _materials;

@synthesize price = _price;

@end
