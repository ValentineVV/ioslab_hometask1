//
//  Stool.m
//  Task2
//
//  Created by Valiantsin Vasiliavitski on 3/27/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import "Stool.h"

@implementation Stool

-(instancetype)initWithLegsAmmount:(NSInteger)legsAmmount back:(BOOL)back  price:(NSInteger) price weight:(NSInteger) weight materials:(Material*) materials {
    
    if (self = [super init]) {
        
        _price = price;
        _materials = materials;
        _weight = weight;
        _legsAmmount = legsAmmount;
        _back = back;
    }
    
    return self;
}

+(instancetype) stoolWithLegsAmmount: (NSInteger) legsAmmount back:(BOOL) back price:(NSInteger) price weight:(NSInteger) weight materials:(Material*) materials {
    
    return [[self alloc] initWithLegsAmmount:legsAmmount back:back price:price weight:weight materials:materials];
}

-(NSString *)description {
    
    return [NSString stringWithFormat:@"Стул: Количество ног: %ld, наличие спинки: %d, цена: %ld, вес: %ld, %@", _legsAmmount, _back, _price, _weight, _materials];
}

@synthesize materials = _materials;

@synthesize weight = _weight;

@synthesize price = _price;

@end
