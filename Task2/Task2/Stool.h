//
//  Stool.h
//  Task2
//
//  Created by Valiantsin Vasiliavitski on 3/27/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FurnitureSet.h"

@interface Stool : NSObject <Characteristics>

@property (nonatomic, readonly) NSInteger legsAmmount;
@property (nonatomic ,readonly) BOOL back;

-(instancetype) initWithLegsAmmount: (NSInteger) legsAmmount back:(BOOL) back price:(NSInteger) price weight:(NSInteger) weight materials:(Material*) materials;
+(instancetype) stoolWithLegsAmmount: (NSInteger) legsAmmount back:(BOOL) back price:(NSInteger) price weight:(NSInteger) weight materials:(Material*) materials;

-(NSString *)description;

@end
