//
//  Bed.h
//  Task2
//
//  Created by Valiantsin Vasiliavitski on 3/27/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FurnitureSet.h"

@interface Bed : NSObject <Characteristics>

@property (nonatomic, readonly, assign) NSInteger springsAmmount;
@property (nonatomic, readonly, assign) NSInteger capacity;

-(instancetype) initWithSpringsAmmount: (NSInteger) springsAmmount capacity:(NSInteger) capacity price:(NSInteger) price weight:(NSInteger) weight materials:(Material*) materials;
+(instancetype) bedWithSpringsAmmount: (NSInteger) springsAmmount capacity:(NSInteger) capacity price:(NSInteger) price weight:(NSInteger) weight materials:(Material*) materials;

-(NSString *)description;

@end
