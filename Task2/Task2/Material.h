//
//  Material.h
//  Task2
//
//  Created by Valiantsin Vasiliavitski on 3/27/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Material : NSObject

@property (nonatomic ,readonly) NSString *name;
@property (nonatomic, readonly) NSString *color;

-(instancetype) initWithName: (NSString*) name color: (NSString *) color;
+(instancetype) materialWithName: (NSString*) name color: (NSString *) color;

-(NSString *)description;

-(BOOL)isEqual:(id)object;

-(BOOL)isEqualToMaterial:(Material*)object;

-(NSUInteger)hash;


@end
