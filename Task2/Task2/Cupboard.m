//
//  Cupboard.m
//  Task2
//
//  Created by Valiantsin Vasiliavitski on 3/27/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import "Cupboard.h"

@implementation Cupboard

-(instancetype)initWithDoorsAmmount:(NSInteger)doorsAmmount shelvesAmmount:(NSInteger)shelvesAmmmount price:(NSInteger)price weight:(NSInteger)weight materials:(Material*)materials {
    
    if (self = [super init]) {
        
        _price = price;
        _materials = materials;
        _weight = weight;
        _doorsAmmount = doorsAmmount;
        _shelvesAmmount = shelvesAmmmount;
    }
    
    return self;
}

+(instancetype)cupboardWithDoorsAmmount:(NSInteger)doorsAmmount shelvesAmmount:(NSInteger)shelvesAmmmount price:(NSInteger)price weight:(NSInteger)weight materials:(Material*)materials {
    
    return [[self alloc] initWithDoorsAmmount:doorsAmmount shelvesAmmount:shelvesAmmmount price:price weight:weight materials:materials];
}

-(NSString *)description {
    
    return [NSString stringWithFormat:@"Шкаф: Количество дверей: %ld, количество полок: %ld, цена: %ld, вес: %ld, %@", _doorsAmmount, _shelvesAmmount, _price, _weight, _materials];
}

@synthesize weight = _weight;

@synthesize materials = _materials;

@synthesize price = _price;

@end
