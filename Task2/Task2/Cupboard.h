//
//  Cupboard.h
//  Task2
//
//  Created by Valiantsin Vasiliavitski on 3/27/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FurnitureSet.h"

@interface Cupboard : NSObject <Characteristics>

@property (nonatomic, readonly) NSInteger doorsAmmount;
@property (nonatomic, readonly) NSInteger shelvesAmmount;

-(instancetype) initWithDoorsAmmount: (NSInteger) doorsAmmount shelvesAmmount: (NSInteger) shelvesAmmmount price:(NSInteger)price weight:(NSInteger)weight materials:(Material*)materials;
+(instancetype) cupboardWithDoorsAmmount: (NSInteger) doorsAmmount shelvesAmmount: (NSInteger) shelvesAmmmount price:(NSInteger)price weight:(NSInteger)weight materials:(Material*)materials;

-(NSString *)description;

@end
