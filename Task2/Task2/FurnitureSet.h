//
//  FurnitureSet.h
//  Task2
//
//  Created by Valiantsin Vasiliavitski on 3/27/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Characteristics.h"
#import "Material.h"
@interface FurnitureSet : NSObject <Characteristics>

@property (nonatomic, readonly, copy) NSMutableArray<id<Characteristics>>* items;


-(instancetype) initWithItem:(id<Characteristics>) item;
+(instancetype) FurnitureSetWithItem:(id<Characteristics>) item;

-(void) addItem: (id) item;

-(NSArray*) includedMaterials;

-(NSInteger) totalWeight;

-(NSInteger) totalPrice;

-(NSSet*) itemsWithMaterial:(Material*) material;

-(NSString *)description;


@end
