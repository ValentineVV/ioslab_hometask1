//
//  Chair.h
//  Task2
//
//  Created by Valiantsin Vasiliavitski on 3/27/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FurnitureSet.h"
@interface Chair : NSObject <Characteristics>

@property (nonatomic, readonly) BOOL casing;
@property (nonatomic, readonly) NSString* filler;

-(instancetype) initWithCasing: (BOOL) casing filler: (NSString*) filler price:(NSInteger) price weight:(NSInteger) weight materials:(Material*) materials;
+(instancetype) chairWithCasing: (BOOL) casing filler: (NSString*) filler price:(NSInteger) price weight:(NSInteger) weight materials:(Material*) materials;

-(NSString *)description;

@end
