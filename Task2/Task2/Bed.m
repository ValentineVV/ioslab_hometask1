//
//  Bed.m
//  Task2
//
//  Created by Valiantsin Vasiliavitski on 3/27/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import "Bed.h"

@implementation Bed

-(instancetype) initWithSpringsAmmount:(NSInteger)springsAmmount capacity:(NSInteger)capacity price:(NSInteger)price weight:(NSInteger)weight materials:(Material*)materials {
    
    if (self = [super init]) {
        
        _price = price;
        _materials = materials;
        _weight = weight;
        _springsAmmount = springsAmmount;
        _capacity = capacity;
    }
    
    return self;
}

+(instancetype)bedWithSpringsAmmount:(NSInteger)springsAmmount capacity:(NSInteger)capacity price:(NSInteger)price weight:(NSInteger)weight materials:(Material*)materials {
    
    return [[self alloc] initWithSpringsAmmount:springsAmmount capacity:(NSInteger)capacity price:price weight:weight materials:materials];
}

-(NSString *)description {
    
    return [NSString stringWithFormat:@"Кровать: Количество пружин: %ld, вместительность: %ld, цена: %ld, вес: %ld, %@", _springsAmmount, (long)_capacity, _price, _weight, _materials];
}

@synthesize materials = _materials;

@synthesize price = _price;

@synthesize weight = _weight;

@end
